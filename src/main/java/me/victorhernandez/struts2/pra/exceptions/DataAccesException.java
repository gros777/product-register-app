package me.victorhernandez.struts2.pra.exceptions;

public class DataAccesException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3956377338442549508L;
	public DataAccesException(String reason){
		super(reason);
	}
}

﻿INSERT INTO PUBLIC.PUBLIC.PRODUCT (ID,PRODUCTNAME,MARK,SHELVE,AVAILABLES,GOODTHOUGH,VERSION) VALUES (
1,'Canelones','La Italiana','OC3409',345,TO_DATE('2014-03-08 21:08:08','YYYY-MM-DD HH24:MI:SS'),44
,2,'Leche entera','Salud','TR5308',56,TO_DATE('2013-08-08 21:08:08','YYYY-MM-DD HH24:MI:SS'),3
,3,'Sal de ajo','McKormik','AB5235',12,TO_DATE('2013-05-13 21:08:08','YYYY-MM-DD HH24:MI:SS'),5
,4,'Café','La Fincona','OC3425',123,TO_DATE('2014-11-24 21:08:08','YYYY-MM-DD HH24:MI:SS'),23
,5,'Yougurt','Yes','RE5678',134,TO_DATE('2014-07-15 01:00:00','YYYY-MM-DD HH24:MI:SS'),1
,6,'Guitarra Clasica','Yamaha','YU2390',45,TO_DATE('2025-12-01 01:00:00','YYYY-MM-DD HH24:MI:SS'),1
,7,'Olive Oil','Olio Sasso','HO2398',86,TO_DATE('2014-10-12 01:00:00','YYYY-MM-DD HH24:MI:SS'),0
,8,'Large Notebook','Moleskine','MO3498',156,TO_DATE('2025-10-12 01:00:00','YYYY-MM-DD HH24:MI:SS'),0
,9,'Canelones Mod','Fama','OC3409',345,TO_DATE('2014-03-08 21:08:08','YYYY-MM-DD HH24:MI:SS'),0
,10,'Audifonos','Klip','MH8596',12,TO_DATE('2025-12-31 01:00:00','YYYY-MM-DD HH24:MI:SS'),0
);